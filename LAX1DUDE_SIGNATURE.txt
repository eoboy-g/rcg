-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Eaglercraft 1.5.2 - Service Pack 1.01

Released: October 27th, 2023 - "23w43a"

New in Service Pack 1:

Huge FPS boosts, lessons learned during the development of EaglercraftX 1.8
have been applied to 1.5, and ayunami2000 added basic support for resource
packs. Update your unblocked game websites ASAP for the best user experience
on slow hardware.

  - fixed sunrise/sunset fog color
  - better optimized pipeline shader
  - backported X's state management
  - backported X's buffer streaming
  - backported X's hotbar FPS fix
  - backported X's double buffering
  - backported X's FXAA shader

Made by lax1dude

Run "GetRepositorySignature.bat", and verify that this is the result:


~~~ Repository SHA-256: ~~~

     11970c765400f505
     739c3581523fffe2
     782b39675871077f
     f0b187b0bfd5ba9f

~~~~~~~~~~~~~~~~~~~~~~~~~~~


Additionally, verify that the following files have the following SHA-1 checksums:

GetRepositorySignature.jar: DCE2D7FDA9065B2475E5894E5D0960B57B03C661
GetRepositorySignature.bat: 4B995076348E03A5322F3EB6AF72948E4C27CEB6
GetRepositorySignature.sh: 8B269BA0D54C2B4BD7E9199B10C9769D39F00DC6


If this is all correct, then this is the official Eaglercraft 1.5.2 Service Pack 1.01


You can download a clean copy of "GetRepositorySignature.jar" here: [https://deev.is/eagler/GetRepositorySignature.jar]

Use "GetRepositorySignature.sh" in place of "GetRepositorySignature.bat" on linux


run "java -jar GetRepositorySignature.jar" to launch a GUI version of the tool,
which allows you to select the repository to check using a file chooser


To verify the PGP signature on this message, my public key is here: [https://deev.is/certs/LAX1DUDE_eagler_public.asc]

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEcBgAq2s4AHU8tLsdcu68cJImpGoFAmV1bG8ACgkQcu68cJIm
pGrx7w/9EGKIQCRBB6SxHeuQ/XIovdzlxtbWm/EprtCR3Qch1X0dkaTDZIb6nE2D
2vkiolOPc8jtJk0AuR6PPIaaA1/dcwRPT02DqPxZeq8R7zesHMfxlNMMLLxfHY6s
+r0SShcncVjJxCsLYGBnyg4UReEStaMvyts/zFtmZwHhjdERiJIBKfEQGnNAOLja
Rb1kLqhpQ7B6rewgzIR52hVzabMh+a2dWBf2XorRYujhgpOD35jEqOdt5j/nc2s0
EMfC11MRUdrxyL7j8ecqZFqLf/TLPvNhyp6h82E82ixdmWV8XjEUkqSaV+KgleBv
y/E4SCAOyffpA3i7jzBSZm66HtpMlQtD/y6i6o/5WQLntyiPA+w7V6PSJ8UzsLna
RHe/KqBwd+7I0w4LWwCaDkBp1HkhdHmAJ7a0DQGk3TztHtG0DJPsoZnI4X644b7e
9BOmnjdX7tHRbIPxlUaem4PXQdcWmWE9qaPhYjKXkPm/MaVUf2zBCSw9K7CSpEMs
c5g2SZs9oZhXmIzG40SALVkICnVdA03i4PWGeMRrJhD0FSm6XjGk/MMFn9O5noum
Cfy8kqZ9zc/cfiF4n18E3AwCTNgl02MLyI70UquiEujRQGc/HoVLKsXOKA6VIEfY
vbvc5fd1od+BgB5H9+PT6Cx1Y4F6TIB2a5gckQJxBX2nkB0d8To=
=K0tf
-----END PGP SIGNATURE-----
